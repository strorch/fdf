/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readf.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 14:31:24 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/12 14:31:26 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static t_line	*readf(char **argv)
{
	int		fd;
	t_line	*new;
	char	*tmp;
	int		res;

	res = 1;
	fd = open(argv[1], O_RDONLY);
	if (fd == -1)
		print_err(argv[1]);
	while (res != 0)
	{
		res = ft_get_next_line(fd, &tmp);
		if (res == -1)
			print_wf(argv[1]);
		if (res != 0)
		{
			ft_list_push_back(&new, ft_strdup(tmp));
			ft_strdel(&tmp);
		}
		else
			free(tmp);
	}
	if (test_check_symb(new))
		print_wf(argv[1]);
	return (new);
}

static int		*return_points(char **line)
{
	int *points;
	int i;

	i = -1;
	points = (int *)malloc(sizeof(int) * count_lines(line));
	while (line[++i] != 0)
		points[i] = ft_atoi(line[i]);
	ft_del_str(line);
	return (points);
}

static void		arr_points(t_coors *coors, t_line *list, char *str)
{
	int		num_rows;
	int		**z_coors;
	int		i;
	t_line	*tmp;

	i = -1;
	num_rows = ft_count(list);
	z_coors = (int **)malloc(sizeof(int *) * num_rows);
	check_count_lines(list, str);
	coors->x = count_lines_free(ft_strsplit(list->data, ' '));
	while (list)
	{
		z_coors[++i] = return_points(ft_strsplit(list->data, ' '));
		free(list->data);
		tmp = list->next;
		free(list);
		list = tmp;
	}
	coors->z = z_coors;
	coors->y = num_rows;
}

t_point			**create_arr_point(t_coors coors,
									int size, int sh_x, int sh_y)
{
	int		i;
	int		j;
	t_point	**map;

	i = -1;
	map = (t_point **)malloc(sizeof(t_point *) * coors.y);
	while (++i < coors.y)
	{
		j = -1;
		map[i] = (t_point *)malloc(sizeof(t_point) * coors.x);
		while (++j < coors.x)
		{
			map[i][j].x = j * size - sh_x;
			map[i][j].y = i * size - sh_y;
			if (coors.z[i][j] > 500)
				map[i][j].z = 500;
			else
				map[i][j].z = coors.z[i][j];
		}
	}
	return (map);
}

t_main			main_struct(char **argv)
{
	t_main	main_coor;
	t_coors	coors;
	t_line	*line;

	line = readf(argv);
	arr_points(&coors, line, argv[0]);
	main_coor.size = 30;
	main_coor.shifts.x = (coors.x - 1) * main_coor.size / 2;
	main_coor.shifts.y = (coors.y - 1) * main_coor.size / 2;
	main_coor.x = coors.x;
	main_coor.y = coors.y;
	main_coor.arr = create_arr_point(coors,
			main_coor.size, main_coor.shifts.x, main_coor.shifts.y);
	main_coor.rot = (t_point){ANGLE_X, ANGLE_Y, ANGLE_Z};
	main_coor.koef = (t_point){0, 0, 0};
	main_coor.color = 0;
	free_coors(coors);
	return (main_coor);
}
