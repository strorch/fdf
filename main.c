/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 16:51:27 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/16 13:14:29 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	add_min_max(t_main *new)
{
	int		i;
	int		j;
	int		min;
	int		max;
	t_zcoor	min_max;

	min = new->arr[0][0].z;
	max = new->arr[0][0].z;
	i = -1;
	while (++i < new->y)
	{
		j = -1;
		while (++j < new->x)
		{
			if (min >= new->arr[i][j].z)
				min = new->arr[i][j].z;
			if (max <= new->arr[i][j].z)
				max = new->arr[i][j].z;
		}
	}
	min_max.min = min;
	min_max.max = max;
	new->min_max = min_max;
}

void	transform_and_draw(t_main new)
{
	mlx_clear_window(new.mlx_ptr, new.win_ptr);
	add_min_max(&new);
	draw_squares(new);
	draw_debug(new, new.mlx_ptr, new.win_ptr);
}

int		ff(int key_code, t_main *new)
{
	char	e;

	e = 0;
	(key_code == 53) ? exit(1) : 0;
	(key_code == 13 && ++e) ? (new->rot.x -= 5) : 0;
	(key_code == 2 && ++e) ? (new->rot.y -= 5) : 0;
	(key_code == 1 && ++e) ? (new->rot.x += 5) : 0;
	(key_code == 0 && ++e) ? (new->rot.y += 5) : 0;
	(key_code == 12 && ++e) ? (new->rot.z += 5) : 0;
	(key_code == 14 && ++e) ? (new->rot.z -= 5) : 0;
	(key_code == 124 && ++e) ? (new->koef.x += 10) : 0;
	(key_code == 123 && ++e) ? (new->koef.x -= 10) : 0;
	(key_code == 126 && ++e) ? (new->koef.y -= 10) : 0;
	(key_code == 125 && ++e) ? (new->koef.y += 10) : 0;
	(key_code == 27 && ++e && new->size > 0) ? (new->size -= 3) : 0;
	(key_code == 24 && ++e) ? (new->size += 3) : 0;
	(key_code == 36 && ++e) ? (new->color += 1) : 0;
	if (e)
		transform_and_draw(*new);
	return (0);
}

int		main(int argc, char **argv)
{
	t_main new;

	if (argc != 2)
		print_usage();
	new = main_struct(argv);
	print_instruction();
	new.mlx_ptr = mlx_init();
	new.win_ptr = mlx_new_window(new.mlx_ptr, HEIGHT, WIDTH, "FDF");
	transform_and_draw(new);
	mlx_hook(new.win_ptr, 2, 0, ff, &new);
	mlx_hook(new.win_ptr, 17, 0, (int (*)())exit, NULL);
	mlx_loop(new.mlx_ptr);
	return (0);
}
