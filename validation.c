/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/13 20:42:04 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/13 20:42:05 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			test_check_symb(t_line *list)
{
	int i;

	while (list)
	{
		i = -1;
		while (list->data[++i] != 0)
			if (list->data[i] != ' ' && ft_isdigit(list->data[i]) == 0)
			{
				if ((list->data[i] == '+'
					|| list->data[i] == '-')
					&& ft_isdigit(list->data[i + 1]) == 1)
					continue ;
				return (1);
			}
		list = list->next;
	}
	return (0);
}

int			count_lines(char **str)
{
	int k;

	k = -1;
	while (str[++k] != 0)
		;
	return (k);
}

int			count_lines_free(char **str)
{
	int k;

	k = -1;
	while (str[++k] != 0)
		;
	ft_del_str(str);
	return (k);
}

void		free_coors(t_coors coors)
{
	int i;

	i = -1;
	while (++i < coors.y)
		free(coors.z[i]);
	free(coors.z);
}

void		free_main(t_main main)
{
	int i;

	i = -1;
	while (++i < main.y)
		free(main.arr[i]);
	free(main.arr);
}
