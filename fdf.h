/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 13:30:00 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/16 16:33:22 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "minilibx_macos/mlx.h"
# include "libft/libft.h"
# include <math.h>

# define HEIGHT		1000
# define WIDTH		1000
# define ANGLE_X	-20
# define ANGLE_Y	20
# define ANGLE_Z	-20
# define RAD		M_PI / 180
# define DEG		180 / M_PI

typedef struct	s_line
{
	char			*data;
	struct s_line	*next;
}				t_line;

typedef struct	s_coors
{
	int x;
	int y;
	int **z;
}				t_coors;

typedef struct	s_point
{
	float	x;
	float	y;
	float	z;
}				t_point;

typedef struct	s_zcoor
{
	int			min;
	int			max;
}				t_zcoor;

typedef struct	s_zpoint
{
	int			first;
	int			second;
}				t_zpoint;

typedef struct	s_main
{
	int			x;
	int			y;
	t_point		**arr;
	t_point		rot;
	t_point		koef;
	t_point		shifts;
	int			size;
	void		*mlx_ptr;
	void		*win_ptr;
	t_zcoor		min_max;
	int			color;
}				t_main;

typedef struct	s_color
{
	int			f;
	int			s;
}				t_color;

void			check_count_lines(t_line *list, char *str);
void			print_wf(char *str);
void			print_instruction(void);
void			print_usage(void);
void			print_err(char *str);
int				count_lines_free(char **str);
t_point			n_p(t_main p, t_point new);
t_point			**create_arr_point(t_coors coors,
									int size, int sh_x, int sh_y);
t_color			r_c(t_main *main);
int				get_mid_color(int start, int end,
								double to_pass, double passed);
void			draw_line(t_main main, t_point first,
							t_point second, t_zpoint point);
void			free_main(t_main main);
void			free_coors(t_coors coors);
t_point			rotate_ort(t_point point, t_point rot);
t_point			mv_point(t_point point, int sh_x, uint sh_y);
void			draw_debug(t_main main, void *mlx_ptr, void *win_ptr);
t_main			zoomed(t_main point);
int				test_check_symb(t_line *list);
void			*free_lst(t_line *list);
void			draw_squares(t_main points);
int				count_lines(char **str);
t_main			t_main_copy(t_main old);
int				ft_count(t_line *begin_list);
void			ft_list_push_back(t_line **begin_list, void *data);
t_main			main_struct(char **argv);

#endif
