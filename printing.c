/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printing.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/13 20:41:22 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/13 20:41:26 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	help_string_draw(int location, char *str1, int to_itoa, t_main main)
{
	char *str;
	char *itoated;

	itoated = ft_itoa(to_itoa);
	str = ft_strjoin(str1, itoated);
	mlx_string_put(main.mlx_ptr, main.win_ptr, 5, location, 0xFFFF00, str);
	free(itoated);
	free(str);
}

int		tmp_check(int rotate)
{
	int tmp;

	tmp = rotate;
	while (tmp >= 360)
		tmp -= 360;
	while (tmp <= -360)
		tmp += 360;
	return (tmp);
}

void	draw_debug(t_main main, void *mlx_ptr, void *win_ptr)
{
	mlx_string_put(mlx_ptr, win_ptr, 5, 5, 0xFFFF00, "FDF info:");
	mlx_string_put(mlx_ptr, win_ptr, 5, 25, 0xFFFF00, "Author - mstorcha");
	help_string_draw(45, "Angle X = ", tmp_check(main.rot.x), main);
	help_string_draw(65, "Angle Y = ", tmp_check(main.rot.y), main);
	help_string_draw(85, "Angle Z = ", tmp_check(main.rot.z), main);
	help_string_draw(105, "Length = ", main.size, main);
}

void	draw_line(t_main main, t_point first,
							t_point second, t_zpoint point)
{
	int		dx;
	int		dy;
	int		dz;
	float	k;
	float	t;

	k = 0.1 / sqrt((second.x - first.x) * (second.x - first.x)
		+ (second.y - first.y) * (second.y - first.y));
	t = -k;
	while ((t += k) <= 1)
	{
		dx = first.x + (second.x - first.x) * t;
		dy = first.y + (second.y - first.y) * t;
		dz = point.first + (point.second - point.first) * t;
		mlx_pixel_put(main.mlx_ptr, main.win_ptr, dx, dy,
			get_mid_color(r_c(&main).f, r_c(&main).s,
				(abs(main.min_max.max - main.min_max.min) == 0)
					? 1 : abs(main.min_max.max - main.min_max.min), abs(dz)));
	}
}

void	draw_squares(t_main p)
{
	int		i;
	int		j;
	t_main	new;

	i = -1;
	new = zoomed(p);
	while (++i < p.y)
	{
		j = -1;
		while (++j < p.x)
		{
			if (j + 1 < p.x)
				draw_line(p, n_p(p, new.arr[i][j]), n_p(p, new.arr[i][j + 1]),
					(t_zpoint){p.arr[i][j].z, p.arr[i][j + 1].z});
			if (i + 1 < p.y)
				draw_line(p, n_p(p, new.arr[i][j]), n_p(p, new.arr[i + 1][j]),
					(t_zpoint){p.arr[i][j].z, p.arr[i + 1][j].z});
		}
	}
	free_main(new);
}
