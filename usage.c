/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 15:43:25 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/16 16:33:39 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	print_instruction(void)
{
	ft_putstr("------FDF Instructions------\n");
	ft_putstr("To move map use arrow keys!\n");
	ft_putstr("To rotate map use WASDQE keys!\n");
	ft_putstr("To zoom map use +- keys!\n");
	ft_putendl("To change map color use 'Enter' key!");
}

void	print_usage(void)
{
	ft_putstr("Usage: ./fdf <filename>\n");
	ft_putendl("Draws a map from file");
	exit(1);
}

void	print_err(char *str)
{
	ft_putstr("Error: ");
	ft_putstr(str);
	ft_putendl(" - No such file or directory!");
	exit(1);
}

void	print_wf(char *str)
{
	ft_putstr("Error: ");
	ft_putstr(str);
	ft_putendl(" - Wrong file or directory!");
	exit(1);
}

void	check_count_lines(t_line *list, char *str)
{
	int count;

	count = count_lines_free(ft_strsplit(list->data, ' '));
	while (list)
	{
		if (count != count_lines_free(ft_strsplit(list->data, ' ')))
			print_wf(str);
		count = count_lines_free(ft_strsplit(list->data, ' '));
		list = list->next;
	}
}
